# Remplissage fonctionnel d'un Array Rust

Présentation d'un exemple de *beau* code Rust.

Beau code = bon code.
Minimal, clair, avec peut de risque de bug, lisible.

## License
La presentation est sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

## Dons
Je peux recevoir les dons sur la plateforme [Liberapay](https://liberapay.com/gwenlg)

## Preview
[html](https://shares.gwenlg.fr/functional-array-filling.html)
