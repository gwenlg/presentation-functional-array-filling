INPUT=presentation.md
OUTPUT=functional-array-filling.html

all: $(OUTPUT)

$(OUTPUT): $(INPUT)
	marp -o $(OUTPUT) $(INPUT)
