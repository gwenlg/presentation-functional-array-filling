---
marp: true
theme: gaia
class: invert
paginate: true
footer: 'By [Gwen Lg](https://gwenlg.fr) - Licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)'
---
# Remplissage fonctionnel d'un Array
Great Rust code
## ![](resources/Rust_programming_language_black_logo.svg)

<!-- Qui je suis ?
- programmeur C++
- expérience jeux vidéo + engine/tools
- habitué à la programmation *imperative*
- je me forme à Rust depuis 2 ans

Partage d'une expérience qui est venu appuyer mon resentit que Rust est un **bon** language de programmation pour faire du super code.
-->
---
# C'est quoi du ***super code*** ?
*Contextualisation*
* un programme informatique c'est compliqué
  * multitude de systèmes, d'interdépendances, d'interactions
  * environnement d'exécution incontrôlé
* demande beaucoup de réflexion pour la conception
* toute bonne pratique, aide, outils, ...
  sont à utiliser autant que possible. 
<!--
concevoir un programme c'est **compliqué**
... dès qu'un projet n'est plus trivial

compliqué de penser à tous, tous les cas, tous les liens, à la fois une vue d'ensemble et une vision des détails.
Compliqué de faire avec les "imperfections" du code, l'existant, l'historique, ...
ça peut occuper beaucoup de cerveau.
c'est pour ça qu'une bonne pratique est d'utiliser un maximum d'outils qui permettent de s'alléger la charge mentale.
Et je trouve que Rust permet vraiment ça, et il a probablement été conçu et évolue dans cet esprit. 
C'est notamment pour cela que je l'apprécie énormément.
-->

---
# C'est quoi du ***super code*** ?
*Contextualisation*
* simple et lisible
* structuré
* conçu pour limiter le risque d'erreurs
* vérifié/vérifiable au maximum par des outils
  compilateur, linteur, ...
* vérifiable par des humains

<!--
Du super code, pour moi ça doit être ...
<>
vérifiable par des humains => donc simple, lisible et structuré

 = moins de risque d'erreur/bug 
 
 simple et lisible et bien structuré c'est important pour qu'il soit vérifiable par d'autres personnes
 Autres personne = vous même dans le future
 -->

---
# Le projet [vobsubocr](https://github.com/elizagamedev/vobsubocr#vobsubocr)
* outil de conversion de sous titres vobsub -> srt
  * vobsub = image
  * reconnaissance de texte par OCR
* la compilation affichait un warning (deux fois)
  ```warning: the type `xyz` does not permit being left uninitialized```

<!-- 
- l'exemple se passe dans le projet **vobsubocr** 
que j'ai voulu tester
- lors de la compilation il y avait des warnings
  même warning à deux endroits
J'ai cherché à corriger les warnings.
Pas juste faire en sorte qu'ils n'apparaissent plus. Mais améliorer le code au passage si possible.
 -->


---
# la fonction rgb_palette_to_luminance
```Rust
// type Palette = [Rgb; 16];
fn rgb_palette_to_luminance(palette: &Palette) -> [f32; 16] {
    let mut luminance_palette: [f32; 16] = unsafe { std::mem::MaybeUninit::uninit().assume_init() };
    for (i, x) in palette.iter().enumerate() {
        let r = srgb_to_linear(x[0]);
        let g = srgb_to_linear(x[1]);
        let b = srgb_to_linear(x[2]);
        luminance_palette[i] = 0.2126 * r + 0.7152 * g + 0.0722 * b;
    }
    return luminance_palette;
}
```
<!--
< AFFICHAGE DANS IDE >
On va s'intéresser ici à la fonction rgb_palette_to_luminance dans laquelle apparaissait le warning

Présentation du code :
- nom de la fonction, entrée, sortie,
- variable retour
- boucle de traitement pour calculer les valeurs et la stocker dans la variable.
- utilisation de l'unsafe pour éviter de pré-initialiser les éléments de l'array
-->

---
# la fonction rgb_palette_to_luminance
Le but est de convertir une palette de couleur en une palette de luminance.
* les palettes sont :
  * des primitives [array](https://doc.rust-lang.org/std/primitive.array.html)s (tableau de taille fixe)
  * de taille identique
* un ensemble d'opérations pour calculer une luminance à partir d'une couleur était appliqué à chaque élément de l'entrée, pour obtenir l'élément correspondant de la sortie.

<!--
- **une palette de couleur** c'est une liste de couleurs, 
  l'idée est d'utiliser dans l'image les index vers les couleurs, à la place des couleurs elle même.
  quand une image n'utilise que quelques couleurs, ça occupe beaucoup moins de place (ici 16 couleurs, donc 4 bits suffisent) qui seront utilisées sous forme d'index par les pixels d'une image à la place d'indiquer directement une couleur (ici 24 bits)

array: variable "simple", ne gère d'allocation, de redimensionnement, etc
-->

---
# rgb_palette_to_luminance
* *contraintes de Rust*
  * tous les éléments de l'array doivent être initialisés 
  * Rust ne peut pas s'assurer qu'une boucle for initialise tout les éléments.
* conséquence 
  * initialiser tous les éléments de l'array avant la boucle.
  * même si les valeurs vont être changées ensuite (dans la boucle)

<!-- 
- contraintes :
  - pour la sureté du code
  - surtout que dans ce cas, l'iterateur s'execute sur l'array d'entré, pas sur celui de retour
- conséquence :
  - Rust demande à initialiser tous ...
Ce qui n'est pas idéal/optimal
 -->

---
# rgb_palette_to_luminance
*alternative*
* indiquer à Rust qu'on ne veut pas initialiser les éléments avant la boucle, et qu'on sait ce que l'on fait.
* fait ici avec du unsafe
  * provoque un warning
  ```warning: the type `[f32; 16]` does not permit being left uninitialized```
  * unsafe = moins de garanties par le compilateur sur le bon fonctionnement du code.
<!--  
dire qu'on parle de la variable de retour
vérification par le compilateur
-->

---
# Améliorer
* ***corriger le warning***
  * initialiser l'array avec Default::default()
* ***Mais est-ce que c'est la meilleure solution ?***
  Est-ce du super code ?
  * valeurs initialisées pour rien
  * une proportion non négligeable de code lien (boilerplate)
  Ex: déclaration, initialisation et retour de la variable

<!--
- Default::default() ->
  - pas de problème de performance ici, code appelé qu'une fois
  
- mais est-ce qu'on ne pourrait pas faire mieux ?
- code lien/boilerplate :
  - code sans logique
  - qui sert juste ici à faire le lien entre l'extérieur de la fonction et la logique à l'intérieur
  contrairement au code d'opération (calcul, transformation, etc)
-->

---
# Améliorer ... vraiment
go to code
<!-- 
DEMO LIVE: transformation de la fonction
- retrait de la pré-initialisation
- transformation d'itératif en fonctionnel :
  - enumerate avec correspondance directe index entrée et sortie
  - utilisation de map possible
  - initialisation directe avec les 'bonnes' valeurs
- map sur les itérateurs ne permet pas de construire un array, seulement un Vec
- map existe sur les arrays, suppression de la transformation en itérateur
- suppression de la variable intermédiaire
-->

---
# Améliorer ...
Récapitulatif :
* retrait du unsafe de *non-initialisation*
* approche fonctionnelle avec map
  fonctionnalité [map](https://doc.rust-lang.org/std/primitive.array.html#method.map) aussi implémentée pour les [array](https://doc.rust-lang.org/std/primitive.array.html)s
* retrait de la transformation en itérateur
* retrait du return et donc de la variable *luminance_palette*

---
# Résultat
```Rust
// type Palette = [Rgb; 16];
fn rgb_palette_to_luminance(palette: &Palette) -> [f32; 16] {
    palette.map(|x|{
        let r = srgb_to_linear(x[0]);
        let g = srgb_to_linear(x[1]);
        let b = srgb_to_linear(x[2]);
        0.2126 * r + 0.7152 * g + 0.0722 * b
    })
}
```

---
# Avantages de l'alternative
* code plus concis
  * moins de risques de bugs
  * plus facile à lire
* plus de vérifications par le compilateur
  * le map assure qu'une valeur est généré pour chaque valeur d'entrée
  * l'utilisation d'array assure que le nombre d'éléments est bien respecté.
  * correspondance de la taille des arrays d'entrée et de sortie
<!-- 
- code plus concis :
  - avec moins de *code lien*
  - suppression de double déclaration :
  type de retour de la fonction et variable associé
  plus de correspondance manuelle

parce que moins de chose à lire
... quand on a un peu l'habitude de Rust (ou autre langage similaire)

DÉMO LIVE de changement du type Palette ?
-->

--- 
# Questions ?

<br/><br/><br/>
Infos :
- *Les slides de présentation ont été réalisés à l'aide de [Marp](https://marp.app/)*
- *Les sources de la présentation sont disponible dans un dépots git et hébergé sur [Framagit](https://framagit.org/gwenlg/presentation-functional-array-filling)*

---
# Bonus: deuxième exemple
Une deuxième fonction avait le même warning
* elle a le même 'pattern'
* mais utilise deux [array](https://doc.rust-lang.org/std/primitive.array.html)s en entrée avec un zip 
* et [array](https://doc.rust-lang.org/std/primitive.array.html) n'implémente pas zip.
*Une version à été implémentée (nightly), mais n'était pas satisfaisante niveau performance ([issue #80094](https://github.com/rust-lang/rust/issues/80094)).*
<!--
zip: disponible en nightly puis retiré 

problème de zip: est que la méthode construisait en mémoire un array de tupple.
Même si on ne l'utilisait pas directement.
Coté Iterateur, un zip construit un objet qui permet d'ittérer sur les tupple, mais ne les construit pas réellement.
Sauf dans le cas ou l'on fait un collect
-->

---
# Bonus: deuxième exemple
* alternative : la crate [iter_fixed](https://crates.io/crates/iter_fixed).
```Provides a type and traits for turning collections of fixed size, like arrays, into IteratorFixed which can be used a bit like an ordinary Iterator but with a compile time guaranteed length. This enables us to turn them back into collections of fixed size without having to perform unnecessary checks during run time.```

<!--
Mais la richesse de l'echosysteme rust fait qu'il y a une alternative dans une crate
iter_fixed: cité dans l'issue concernant zip sur map
-->
