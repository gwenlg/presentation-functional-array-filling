use crate::common::*;

/// Generate a binarized palette where `true` represents a filled text pixel.
pub fn binarize_palette(
    palette: &[f32; 16],
    sub_palette: &[u8; 4],
    sub_palette_visibility: &[bool; 4],
    threshold: f32,
) -> [bool; 4] {
    let max_luminance = compute_max_luminance(sub_palette, sub_palette_visibility, palette);
    if max_luminance == 0.0 {
        // Empty image?
        return [false; 4];
    }

    let mut result: [bool; 4] = unsafe { std::mem::MaybeUninit::uninit().assume_init() };
    for (i, (&palette_ix, &visible)) in sub_palette
        .iter()
        .rev()
        .zip(sub_palette_visibility)
        .enumerate()
    {
        result[i] = if visible {
            let luminance = palette[palette_ix as usize] / max_luminance;
            luminance > threshold
        } else {
            false
        }
    }
    result
}
