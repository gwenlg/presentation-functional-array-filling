pub type Rgb = [u8; 3];
pub type Palette = [Rgb; 16];

pub static PALETTE_EXAMPLE: Palette = [
    [0, 0, 0],
    [240, 240, 240],
    [204, 204, 204],
    [153, 153, 153],
    [51, 51, 250],
    [17, 17, 187],
    [250, 51, 51],
    [187, 17, 17],
    [51, 250, 51],
    [17, 187, 17],
    [250, 250, 51],
    [187, 187, 17],
    [250, 51, 250],
    [187, 17, 187],
    [51, 250, 250],
    [17, 187, 187],
];

pub static SUB_PALETTE: [u8; 4] = [0, 3, 2, 0];
pub static SUB_PALETTE_VISIBILITY: [bool; 4] = [false, true, true, true];
pub static THRESHOLD: f32 = 0.6;

/// Convert an sRGB color space channel to linear.
pub fn srgb_to_linear(channel: u8) -> f32 {
    let value = channel as f32 / 255.0;
    if value <= 0.04045 {
        value / 12.92
    } else {
        ((value + 0.055) / 1.055).powf(2.4)
    }
}

pub fn compute_max_luminance(
    sub_palette: &[u8; 4],
    sub_palette_visibility: &[bool; 4],
    palette: &[f32; 16],
) -> f32 {
    // Find the max luminance, so we can scale each luminance value by it.
    // Reminder that the sub palette is reversed.
    let mut max_luminance = 0.0;
    for (&palette_ix, &visible) in sub_palette.iter().rev().zip(sub_palette_visibility) {
        if visible {
            let luminance = palette[palette_ix as usize];
            if luminance > max_luminance {
                max_luminance = luminance;
            }
        }
    }
    max_luminance
}
