mod common;
mod example_1;
mod example_2;
mod solution;

use common::*;
use example_1::rgb_palette_to_luminance;
use example_2::binarize_palette;

fn main() {
    let lum_palette = rgb_palette_to_luminance(&PALETTE_EXAMPLE);
    println!("{:#?}", lum_palette);

    let bin_palette = binarize_palette(
        &lum_palette,
        &SUB_PALETTE,
        &SUB_PALETTE_VISIBILITY,
        THRESHOLD,
    );
    println!("{:#?}", bin_palette);
}
