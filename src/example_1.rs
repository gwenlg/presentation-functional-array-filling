use crate::common::*;

// type Rgb = [u8; 3];
// type Palette = [Rgb; 16];
pub fn rgb_palette_to_luminance(palette: &Palette) -> [f32; 16] {
    let mut luminance_palette: [f32; 16] = unsafe { std::mem::MaybeUninit::uninit().assume_init() };
    for (i, x) in palette.iter().enumerate() {
        let r = srgb_to_linear(x[0]);
        let g = srgb_to_linear(x[1]);
        let b = srgb_to_linear(x[2]);
        luminance_palette[i] = 0.2126 * r + 0.7152 * g + 0.0722 * b;
    }
    return luminance_palette;
}
