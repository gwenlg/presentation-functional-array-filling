use iter_fixed::IntoIteratorFixed;

use crate::common::*;

pub fn _rgb_palette_to_luminance(palette: &Palette) -> [f32; 16] {
    palette.map(|x| {
        let r = srgb_to_linear(x[0]);
        let g = srgb_to_linear(x[1]);
        let b = srgb_to_linear(x[2]);
        0.2126 * r + 0.7152 * g + 0.0722 * b
    })
}

/// Generate a binarized palette where `true` represents a filled text pixel.
pub fn _binarize_palette(
    palette: &[f32; 16],
    sub_palette: &[u8; 4],
    sub_palette_visibility: &[bool; 4],
    threshold: f32,
) -> [bool; 4] {
    let max_luminance = compute_max_luminance(sub_palette, sub_palette_visibility, palette);
    if max_luminance == 0.0 {
        // Empty image?
        return [false; 4];
    }

    sub_palette
        .into_iter_fixed()
        .rev()
        .zip(sub_palette_visibility)
        .map(|(&palette_ix, &visible)| {
            if visible {
                let luminance = palette[palette_ix as usize] / max_luminance;
                luminance > threshold
            } else {
                false
            }
        })
        .collect()
}
